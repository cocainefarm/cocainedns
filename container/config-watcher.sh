#!/usr/bin/env sh

BIND_CONFIG_DIR=${BIND_CONFIG_DIR:-"/var/lib/pdns"}

NAME="$1"

if [ "$2" = "primary" ]; then
    target="$NAME-secondary"
elif [ "$2" = "secondary" ]; then
    target="$NAME-primary"

    [ ! -f "$BIND_CONFIG_DIR/autoprimary.conf" ] && touch "$BIND_CONFIG_DIR/autoprimary.conf"
    [ ! -f "$BIND_CONFIG_DIR/named-autosecondary.conf" ] && touch "$BIND_CONFIG_DIR/named-autosecondary.conf"

    mkdir -p "$BIND_CONFIG_DIR/zones"
else
    echo "Usage: $0 NAME [primary|secondary]"
    exit 1
fi

echo "Starting to watch '$target'"

while [ ! -S "/var/run/pdns/pdns.controlsocket" ]; do
    echo "waiting on pdns to become available"
    sleep 1
done

while true; do
    kubectl get pods -o custom-columns=IP:.status.podIP -w --no-headers --field-selector=status.phase=Running -l name="$target" |
        while read -r ip; do
            if [ "$2" = "primary" ]; then
                echo "pod $ip became ready, notifying"
                pdns_control notify '*'
            elif [ "$2" = "secondary" ]; then
                echo "switching autoprimary to $ip"
                echo "$ip" >"$BIND_CONFIG_DIR/autoprimary.conf"
                if [ ! -f "/tmp/kubectl_killed" ]; then
                    pdns_control cycle
                fi
            fi

            if [ -f "/tmp/kubectl_killed" ]; then
                rm -f /tmp/kubectl_killed
            fi
        done
    echo "kubectl got killed... restarting"
    touch /tmp/kubectl_killed
done
