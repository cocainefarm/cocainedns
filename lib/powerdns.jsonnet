{
  _config+:: {
    pdns: {
      name: 'pdns',
      image: {
        repo: 'docker.io/powerdns/pdns-auth-46',
        tag: '4.6.1',
      },
      sidecar: {
        image: {
          repo: 'kube.cat/cocainefarm/pdns-sidecar',
          tag: 'latest',
        },
      },
      admin: {
        image: {
          repo: 'docker.io/powerdnsadmin/pda-legacy',
          tag: 'latest',
        },
      },
      primary: {
        // primaryDomain: "",
        allowedIPs: '10.0.0.0/8,127.0.0.0/8',
        // apikey: '',
        // alsoNotify: '',
        db: {
          // host: "",
          // port: "",
          // name: "",
          // user: "",
          // password: "",
        },
        configMap: {},
      },
      secondary: {
        configMap: {},
      },
    },
  },

  local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet',
  local statefulset = k.apps.v1.statefulSet,
  local deployment = k.apps.v1.deployment,
  local daemonset = k.apps.v1.daemonSet,
  local container = k.core.v1.container,
  local env = k.core.v1.envVar,
  local port = k.core.v1.containerPort,
  local service = k.core.v1.service,
  local servicePort = k.core.v1.servicePort,

  local util = import 'util/main.libsonnet',

  local cfg = $._config.pdns,

  local configMap(name, default, extra) = k.core.v1.configMap.new(name, {
    'pdns.conf': std.manifestIni({ main: default, sections: {} }),
  } + extra),

  local name(name) = { name: name },

  pdns: {
    primaryConfig:: {
      api: 'yes',
      primary: 'yes',
      guardian: 'yes',

      webserver: 'yes',
      'webserver-address': '0.0.0.0',
      'webserver-allow-from': cfg.primary.allowedIPs,

      'local-address': '0.0.0.0 [::]',

      'default-soa-content': '%s hostmaster.@ 0 10800 3600 604800 3600' % cfg.primary.primaryDomain,
      'allow-dnsupdate-from': cfg.primary.allowedIPs,
      'allow-axfr-ips': cfg.primary.allowedIPs,
      'disable-syslog': 'yes',

      'only-notify': '',
      'also-notify': cfg.primary.alsoNotify,

      launch: 'gpgsql',
      'gpgsql-host': cfg.primary.db.host,
      'gpgsql-port': cfg.primary.db.port,
      'gpgsql-dbname': cfg.primary.db.name,
      'gpgsql-user': cfg.primary.db.user,
      'gpgsql-password': cfg.primary.db.password,
      'include-dir': '/etc/powerdns/pdns.d',
    },
    secretApikey: k.core.v1.secret.new('%s-apikey' % cfg.name, {
      'apikey.conf': std.base64('api-key = %s\n' % cfg.primary.apikey),
    }),
    statefulset: statefulset.new(
                   name='%s-primary' % cfg.name
                   , replicas=1
                   , containers=[
                     container.new(
                       'primary'
                       , cfg.image.repo + ':' + cfg.image.tag
                     )
                     + container.withPorts([
                       port.newNamed(8081, 'api'),
                       port.newNamed(53, 'dns-tcp'),
                       port.newNamedUDP(53, 'dns-udp'),
                     ])
                     + container.securityContext.capabilities.withAdd(['NET_BIND_SERVICE'])
                     + container.securityContext.withRunAsUser(0)
                     + container.securityContext.withRunAsGroup(0)
                     + util.commandProbes(['pdns_control', 'rping'], 5, 5, 5, 5),
                     container.new(
                       'config-watcher'
                       , cfg.sidecar.image.repo + ':' + cfg.sidecar.image.tag
                     ) + container.withArgs([cfg.name, 'primary']),
                   ]
                 ) + k.util.emptyVolumeMount('control', '/var/run/pdns')
                 + k.util.configMapVolumeMount(self.primaryConfigmap,
                                               '/etc/powerdns/pdns.conf',
                                               volumeMountMixin=k.core.v1.volumeMount.withSubPath('pdns.conf'))
                 + k.util.secretVolumeMount(self.secretApikey.metadata.name,
                                            '/etc/powerdns/pdns.d/apikey.conf',
                                            defaultMode=292,
                                            volumeMountMixin=k.core.v1.volumeMount.withSubPath('apikey.conf'))
                 + statefulset.spec.withServiceName(self.primaryService.metadata.name)
                 + statefulset.spec.template.spec.withServiceAccountName(self.rbac.service_account.metadata.name),

    primaryConfigmap: configMap(
      '%s-primary-config' % cfg.name,
      self.primaryConfig,
      cfg.primary.configMap
    ),
    primaryService: service.new(
                      self.statefulset.metadata.name,
                      name(self.statefulset.metadata.name),
                      [
                        servicePort.newNamed(name='api', port=8081, targetPort='api'),
                        servicePort.newNamed(name='dns-tcp', port=53, targetPort='dns-tcp'),
                        servicePort.newNamed(name='dns-udp', port=53, targetPort='dns-udp')
                        + servicePort.withProtocol('UDP'),
                      ]
                    ) + service.spec.withIpFamilyPolicy('RequireDualStack')
                    + service.spec.withClusterIP('None'),

    secondaryConfig:: {
      secondary: 'yes',
      autosecondary: 'yes',
      guardian: 'yes',

      'local-address': '0.0.0.0 [::]',

      launch: 'bind',
      'bind-supermaster-config': '/var/lib/pdns/named-autosecondary.conf',
      'bind-supermasters': '/var/lib/pdns/autoprimary.conf',
      // 'bind-autoprimary-destdir': '/var/lib/pdns/zones',
    },
    secondaryConfigmap: configMap(
      '%s-secondary-config' % cfg.name,
      self.secondaryConfig,
      cfg.secondary.configMap
    ),
    daemonset: daemonset.new(
                 name='%s-secondary' % cfg.name
                 , containers=[
                   container.new(
                     'secondary'
                     , cfg.image.repo + ':' + cfg.image.tag
                   )
                   + container.withPorts([
                     port.newNamed(53, 'dns-tcp'),
                     port.newNamedUDP(53, 'dns-udp'),
                   ])
                   + container.securityContext.withRunAsUser(0)
                   + container.securityContext.withRunAsGroup(0)
                   + container.securityContext.capabilities.withAdd(['NET_BIND_SERVICE'])
                   + util.commandProbes(['pdns_control', 'rping'], 5, 5, 5, 5),
                   container.new(
                     'config-watcher'
                     , cfg.sidecar.image.repo + ':' + cfg.sidecar.image.tag
                   ) + container.withArgs([cfg.name, 'secondary']),
                 ]
               ) + k.util.emptyVolumeMount('control', '/var/run/pdns')
               + k.util.configMapVolumeMount(self.secondaryConfigmap,
                                             '/etc/powerdns/pdns.conf',
                                             volumeMountMixin=k.core.v1.volumeMount.withSubPath('pdns.conf'))
               + k.util.hostVolumeMount('bind', '/srv/bind', '/var/lib/pdns')
               + daemonset.spec.template.spec.withServiceAccountName(self.rbac.service_account.metadata.name),
    secondaryService: service.new(
      self.daemonset.metadata.name,
      name(self.daemonset.metadata.name),
      [
        servicePort.newNamed(name='dns-tcp', port=53, targetPort='dns-tcp'),
        servicePort.newNamed(name='dns-udp', port=53, targetPort='dns-udp')
        + servicePort.withProtocol('UDP'),
      ]
    ) + service.spec.withIpFamilyPolicy('RequireDualStack'),

    secondaryServiceHeadless: self.secondaryService
                              + service.metadata.withName('%s-headless' % self.secondaryService.metadata.name)
                              + service.spec.withClusterIP('None')
                              + service.spec.withIpFamilyPolicy('SingleStack'),

    rbac: k.util.namespacedRBAC(cfg.name, [{
      apiGroups: [''],
      resources: ['pods'],
      verbs: ['get', 'watch', 'list'],
    }]),

    admin: deployment.new(
      replicas=1
      , name='%s-admin' % cfg.name
      , containers=[
        container.new(
          'admin'
          , cfg.admin.image.repo + ':' + cfg.admin.image.tag
        ) + container.withPorts([port.newNamed(80, 'http')])
        + container.withEnvFrom(k.core.v1.envFromSource.configMapRef.withName('%s-admin' % cfg.name))
        + container.withEnv([
          env.new(
            'SQLALCHEMY_DATABASE_URI',
            'postgresql://%s:%s@%s/%s' %
            [cfg.primary.db.user, cfg.primary.db.password, cfg.primary.db.host, cfg.primary.db.name]
          ),
        ])
        + util.httpProbes('http', '/', 5, 5, 5, 5),
      ]
    ) + deployment.configVolumeMount('%s-admin' % cfg.name, '/saml'),
    adminConfig: k.core.v1.configMap.new('%s-admin' % cfg.name, {
        "SAML_ENABLED": "True",
        "SAML_DEBUG": "True",
        "SAML_PATH": "os.path.join(os.path.dirname(__file__), 'saml')",
        "SAML_METADATA_URL": "https://auth.vapor.systems/application/saml/powerdns/metadata/",
        "SAML_METADATA_CACHE_LIFETIME": "1",
        "SAML_LOGOUT_URL": "https://auth.vapor.systems/application/saml/powerdns/slo/binding/redirect/",
        "SAML_SP_ENTITY_ID": "pdns-admin",
        "SAML_SP_CONTACT_NAME": "me",
        "SAML_SP_CONTACT_MAIL": "me",
        "SAML_NAMEID_FORMAT": "urn:oasis:names:tc:SAML:2.0:nameid-format:persistent",
        "SAML_ATTRIBUTE_USERNAME": "http://schemas.goauthentik.io/2021/02/saml/username",
        "SAML_ATTRIBUTE_NAME": "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name",
        "SAML_ATTRIBUTE_EMAIL": "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress",
        "SAML_ATTRIBUTE_GROUP": "http://schemas.xmlsoap.org/claims/Group",
        "SAML_GROUP_ADMIN_NAME": "admin",
        "SAML_SIGN_REQUEST": "'False'",
        "SAML_ASSERTION_ENCRYPTED": "False",
        "SAML_WANT_MESSAGE_SIGNED": "False",
        "SAML_CERT": "/saml/saml.crt",
        "saml.crt": importstr "../secrets/kubernetes/ns/saml.crt"
    }),
    adminService: k.util.serviceFor(self.admin),
    adminIngress: util.ingressFor(self.adminService, 'ns.vapor.systems', 'vapor-systems-tls'),
  },
}
