local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet';
local util = import 'util/main.libsonnet';

local tanka = import 'github.com/grafana/jsonnet-libs/tanka-util/main.libsonnet';
local helm = tanka.helm.new(std.thisFile);

(util.inlineSpec('https://ettves.vapor.systems:6443', 'ns', null, null))
+ {
  _config+:: self.data._config,
  data: (import 'powerdns.jsonnet') + {
    postgresqlPassword:: std.stripChars(importstr './secrets/ns/postgresql', '\n'),
    pdnsApiKey:: std.stripChars(importstr './secrets/ns/api', '\n'),

    _config+:: {
      namespace: 'ns',
      pdns+: {
        name: 'pdns',
        image: {
          repo: 'kube.cat/cocainefarm/pdns-auth',
          tag: 'latest',
        },
        primary+: {
          primaryDomain: 'primary.ns.vapor.systems',
          allowedIPs: '10.0.0.0/8,127.0.0.0/8',
          apikey: $.data.pdnsApiKey,
          alsoNotify: 'pdns-secondary-headless.ns.svc.kube.vapor.systems',
          db: {
            host: $.data.postgresql.service_postgresql.metadata.name,
            port: $.data.postgresql.service_postgresql.spec.ports[0].port,
            name: 'pdns',
            user: 'pdns',
            password: $.data.postgresqlPassword,
          },
          configMap: {},
        },
        secondary+: {
          configMap: {},
        },
      },
    },

    pdns+: {
      daemonset+: k.apps.v1.daemonSet.spec.template.spec.withNodeSelector({
        'kubernetes.io/role': 'ns',
      }),

      bgp: {
        local service = k.core.v1.service,
        local servicePort = k.core.v1.servicePort,

        udpV6: service.new(
          'pdns-bgp-v6',
          { name: 'pdns-secondary' },
          [
            servicePort.newNamed(name='dns-udp', port=53, targetPort='dns-udp')
            + servicePort.withProtocol('UDP'),
          ]
        ) + service.metadata.withAnnotationsMixin({
          'metallb.universe.tf/address-pool': 'bgp',
        })
        + service.spec.withLoadBalancerIP('2a0f:9400:8020::100')
        + service.spec.withType('LoadBalancer')
        + service.spec.withAllocateLoadBalancerNodePorts(false)
        + service.spec.withExternalTrafficPolicy('Local')
        + service.spec.withIpFamilyPolicy('SingleStack')
        + service.spec.withIpFamilies(['IPv6']),
        udpV4: service.new(
          'pdns-bgp-v4',
          { name: 'pdns-secondary' },
          [
            servicePort.newNamed(name='dns-udp', port=53, targetPort='dns-udp')
            + servicePort.withProtocol('UDP'),
          ]
        ) + service.metadata.withAnnotationsMixin({
          'metallb.universe.tf/address-pool': 'ns',
        })
        + service.spec.withLoadBalancerIP('217.163.29.14')
        + service.spec.withType('LoadBalancer')
        + service.spec.withAllocateLoadBalancerNodePorts(false)
        + service.spec.withExternalTrafficPolicy('Local')
        + service.spec.withIpFamilyPolicy('SingleStack')
        + service.spec.withIpFamilies(['IPv4']),
      },
    },

    postgresql: helm.template('postgresql', './charts/postgresql', {
      namespace: 'ns',
      values: {
        auth: {
          username: 'pdns',
          password: $.data.postgresqlPassword,
          database: 'pdns',
        },
        replication: {
          enabled: false,
        },
        primary: {
          initdb: {
            user: 'pdns',
            password: $.data.postgresqlPassword,
            scripts: {
              'schema.sql': importstr './lib/pdns.sql',
            },
          },
          persistence: {
            storageClass: 'ssd',
            size: '8Gi',
            enabled: true,
          },
        },
      },
    }),
  },
}
